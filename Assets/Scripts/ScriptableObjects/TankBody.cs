﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewBody",menuName ="Tank/Body")]
public class TankBody : ScriptableObject
{
    [Header("TankBody settings")]
    public new string name = "Body";
    public Sprite bodySprite;
    public Vector2 offset = new Vector2(0f,0.15f);
    public int renderLayer = 5;

    [Header("GamePlay modifiers")]
    public int healthModifier = 0;

    [Header("Buying")]
    public int price = 1;
}
