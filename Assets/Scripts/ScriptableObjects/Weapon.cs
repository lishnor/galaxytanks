﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "Tank/Weapon")]
public class Weapon : ScriptableObject
{
    [Header("Weapon settings")]
    public new string name = "Weapon";
    public Sprite weaponSprite;
    public float weaponSize = 0.2f;
    public float weaponLengthMultiplier = -2f;

    [Header("Projectile settings")]
    public float rangeOfPerfectHit = 0.2f;
    public int damage = 5;
    public float speed = 5;
    public float lifeTime = 5;
    public int cycles = 1;
    public float cycleInterval = 0.5f;

    [Header("Buying")]
    public int price = 1;
    public int amount = 1;
}
