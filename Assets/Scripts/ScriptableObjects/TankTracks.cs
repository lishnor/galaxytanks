﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTrack", menuName = "Tank/Track")]
public class TankTracks : ScriptableObject
{
    [Header("TankTrack settings")]
    public new string name = "Track";
    public Sprite trackSprite;
    public Vector2 offset = Vector2.zero;
    public int renderLayer = 4;

    [Header("GamePlay modifiers")]
    public float speedModifier = 1f;

    [Header("Buying")]
    public int price = 1;
}
