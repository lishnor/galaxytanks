﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBarrel", menuName = "Tank/Barrel")]
public class TankBarrel : ScriptableObject
{
    [Header("TankBarrel settings")]
    public new string name = "Barrel";
    public Sprite barrelSprite;
    public Vector2 offset = new Vector2(-0.054f,0.582f);
    public int renderLayer=4;

    [Header("GamePlay modifiers")]
    public int predictionRange = 5;

    [Header("Buying")]
    public int price = 1;
}
