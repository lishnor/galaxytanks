﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 15f;

    // public Transform planetOrigin;
    public Planet planet;

    [SerializeField] LayerMask layer;
    [SerializeField][Range(0.0f,0.1f)] float dephSkin;
    [SerializeField] float speedSmoothtime = 0.1f;
    float speedSmoothVelocity = 0;
    float currentSpeed;

    float moveInput = 0f;
    [SerializeField][Range(-180f, 180f)]public float angle = 0f;
    public bool isEnemy = false;
    public bool isEnabled = false;

    public static UnityAction<float> OnMove;


    void Start()
    {   
        if (!isEnemy)
        {
            InputManager.PlayerInput.Gameplay.Horizontal.performed += ctx => moveInput = ctx.ReadValue<float>();
            InputManager.PlayerInput.Gameplay.Horizontal.canceled += ctx => moveInput = 0;
        }

        PlaceOnPlanet();
    }

    private void Update()
    {
        Move(moveInput);
    }

    public void Move(float input) 
    {
        if (!isEnemy)
        {
            if(isEnabled)
            {
                if (Mathf.Abs(moveInput) > 0)
                    AudioManager.instance.PlayContinue("TankMove");
                else
                    AudioManager.instance.Pause("TankMove");

                currentSpeed = CalculateSpeed(speed * input);
                angle += currentSpeed * Time.deltaTime;
                if (angle > 180f)
                {
                    angle -= 360f;
                }
                else if (angle < -180f)
                {
                    angle += 360f;
                }
            } else
            {
                currentSpeed = 0f;
            }
        }
        PlaceOnPlanet();
        if (currentSpeed != 0 && !isEnemy)
        {
            Debug.Log("Speed " + currentSpeed);
            OnMove?.Invoke(angle);
        }
    }

    //[ContextMenu("SetDistance")]
    //public void SetDistanceToPlanet() 
    //{
    //    Vector2 castDirection = planetOrigin.position - transform.position;
    //    castDirection.Normalize();

    //    Vector2 castOrigin = transform.position + transform.up * dephSkin*2;
    //    Debug.DrawRay(castOrigin, castDirection, Color.red);
    //    RaycastHit2D hit2D =Physics2D.Raycast(castOrigin, castDirection,100f,layer);

    //    Debug.Log(hit2D.collider?.name +" "+hit2D.point);

    //    transform.position = hit2D.point;
    //    transform.position = transform.position - transform.up * dephSkin;

    //    float angle = Vector2.Angle(Vector2.down, castDirection);

    //    if (castDirection.x < 0 || castDirection.y < 0)
    //    {
    //       angle = -angle;
    //    }
    //    transform.rotation = Quaternion.Euler(0,0,angle);
    //}

    public void PlaceOnPlanet()
    {
        if (!planet) return;
        var angleRadNorm = angle * Mathf.Deg2Rad;
        var radius = planet.radius - dephSkin;
        var oldRotation = transform.rotation;
        var newRotation = Quaternion.Euler(0, 0, -angle);
        var oldPosition = transform.position;
        var newPosition = planet.transform.position;
        newPosition += new Vector3(radius * Mathf.Sin(angleRadNorm), radius * Mathf.Cos(angleRadNorm));
        if (isEnemy)
        {
            if(newRotation != oldRotation || newPosition != oldPosition)
            {
                transform.DOMove(newPosition, 50f / 1000);
                transform.DORotateQuaternion(newRotation, 50f / 1000);
            }
        } else
        {
            transform.position = newPosition;
            transform.rotation = newRotation;
        }
    }

    private float CalculateSpeed(float speed) 
    {
        var damp = Mathf.SmoothDamp(currentSpeed, speed, ref speedSmoothVelocity, speedSmoothtime);
        return Mathf.Abs(damp) < 0.1 ? 0f : damp;
    }
}
