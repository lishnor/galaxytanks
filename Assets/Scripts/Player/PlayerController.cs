﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public int health = 100;
    private void Start()
    {

    }
    private void Update()
    {
        //GetHit(1);
    }
    PlayerMovement _movement;
    PlayerShooting _shooting;
    public UnityAction<float, float, int> OnShoot;
    public UnityAction<float, float> OnMove;
    public UnityAction<int> OnGetHit;
    public UnityAction OnShootingEnd;

    bool moved = false;
    public bool isEnemy;

    void Awake()
    {
        _movement = GetComponent<PlayerMovement>();
        _shooting = GetComponent<PlayerShooting>();
        PlayerMovement.OnMove += TankMove;
        PlayerShooting.OnMove += GunMove;
        _shooting.shootAction += PlayerShoot;
        BattleRoomManager.OnMyTurnStart += OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd += OnMyTurnEnd;
        GetComponentInChildren<ShootingSystemCallbacks>().ParticlesDied += OnParticlesDied;
    }

    void OnMyTurnStart()
    {
        SetEnabled(true);
        _shooting.shotInRound = false;
    }

    void OnMyTurnEnd()
    {
        SetEnabled(false);
    }

    void PlayerShoot()
    {
        OnShoot?.Invoke(_movement.angle, _shooting.gunAngle, 0);
        AudioManager.instance.Play("ShotFire");
    }

    void GunMove(float a)
    {
        moved = true;
    }

    void TankMove(float a)
    {
        moved = true;
    }

    void LateUpdate()
    {
        if (moved)
        {
            OnMove?.Invoke(_movement.angle, _shooting.gunAngle);
            moved = false;
        }
    }

    public void GetHit(int hitAmount) 
    {
        Analytics.CustomEvent("PlayerGotHit",new Dictionary<string, object>() { { "Damage", hitAmount }  });
        //health -= hitAmount;
        Debug.Log("Got Hit "+ hitAmount);
        //healthBar.SetHealth(health);
        OnGetHit?.Invoke(hitAmount);
    }

    public void SetHealth(int newHealth)
    {
        health = newHealth;
    }

    void SetEnabled(bool enabled)
    {
        _movement.isEnabled = enabled;
        _shooting.isEnabled = enabled;
    }

    void OnParticlesDied()
    {
        if (!isEnemy)
        {
            OnShootingEnd?.Invoke();
        }
    }

    void OnDestroy()
    {
        BattleRoomManager.OnMyTurnStart -= OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd -= OnMyTurnEnd;
    }
}
