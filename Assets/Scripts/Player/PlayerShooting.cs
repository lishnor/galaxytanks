﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using static UnityEngine.InputSystem.InputAction;

public class PlayerShooting : MonoBehaviour
{
    [Header("SelectedWeapon")]
    public Weapon weapon;

    [Header("SystemSetup")]
    public ParticleSystem shootingSystem;
    public ParticleSystem locatingSystem;
    [SerializeField] Material projectileMaterial;
    int ammoAmount;
    public int AmmoAmount => ammoAmount;
    public Action shootAction;

    [SerializeField] LayerMask layerMask;

    [Header("Barrel")]
    [SerializeField] Transform barrel;
    [SerializeField] Transform barrelTarget;
    [SerializeField] [Range(-180f, 180f)] public float gunAngle = 0f;
    public bool isEnemy = false;
    public bool isEnabled = false;
    public bool shotInRound = false;

    public static UnityAction<float> OnMove;

    private void Awake()
    {
        shootingSystem.Stop();
    }

    private void Start()
    {
        if(!isEnemy)
        {
            InputManager.PlayerInput.Gameplay.Shoot.performed += OnShoot;
        }

        GetComponentInChildren<ShootingSystemCallbacks>().AnyParticleHit += DealDamage;

        SetupWeapon();
    }

    private void Update()
    {
        RotateBarrel();   
    }

    void OnShoot(CallbackContext ctx)
    {
        Shoot();
    }

    public void Shoot() 
    {
        if ((ammoAmount > 0 || ammoAmount == -1) && isEnabled && !shotInRound || isEnemy)
        {
            if (ammoAmount > 0) ammoAmount--;

            shootAction?.Invoke();
            shootingSystem.Stop();
            shootingSystem.Play();
            shotInRound = true;
        }
    }

    [ContextMenu("SetupWeapon")]
    public void SetupWeapon() 
    {
        ammoAmount = weapon.amount;

        //SetupShooting
        var main = shootingSystem.main;

        main.startSpeed = weapon.speed;
        main.startLifetime = weapon.lifeTime;
        main.startSize = weapon.weaponSize;

        var burst = shootingSystem.emission.GetBurst(0);
        burst.cycleCount = weapon.cycles;
        burst.repeatInterval = weapon.cycleInterval;
        shootingSystem.emission.SetBurst(0, burst);
       
        shootingSystem.GetComponent<ParticleSystemRenderer>().lengthScale = weapon.weaponLengthMultiplier;

        projectileMaterial.SetTexture("_BaseMap", weapon.weaponSprite.texture);

        //SetupLocating
        main = locatingSystem.main;

        main.startSpeed = weapon.speed;
        main.startLifetime = weapon.lifeTime;
        main.startSize = weapon.weaponSize;
        locatingSystem.GetComponent<ParticleSystemRenderer>().lengthScale = weapon.weaponLengthMultiplier;
    }

    public void RotateBarrel() 
    {
        if (!isEnemy)
        {
            Vector2 diff = Camera.main.ScreenToWorldPoint(barrelTarget.position) - barrel.position;
            diff.Normalize();
            gunAngle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        }
        var oldRotation = barrel.rotation;
        var newRotation = Quaternion.Euler(0f, 0f, gunAngle);
        if (oldRotation != newRotation)
        {
            if (isEnemy)
            {
                barrel.DORotateQuaternion(newRotation, 50f / 1000);
            }
            else
            {
                barrel.rotation = newRotation;
                OnMove?.Invoke(gunAngle);
            }
        }
    }

    public void DealDamage(Vector3 position) 
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(position, 1f,Vector2.one,1f,layerMask);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Enemy")) 
            {

                float distance = (hit.distance > 0f) ? hit.distance : weapon.rangeOfPerfectHit;
                float damageMultiplayier = Mathf.Clamp(weapon.rangeOfPerfectHit / hit.distance, 0f, 1f);
                int hitAmount = (int)(weapon.damage * damageMultiplayier);

                hit.collider.GetComponent<PlayerController>().GetHit(hitAmount);

            }
        }

    }

    public void OnDisable()
    {
        GetComponentInChildren<ShootingSystemCallbacks>().AnyParticleHit -= DealDamage;
    }

    void OnDestroy()
    {
        if (!isEnemy)
        {
            InputManager.PlayerInput.Gameplay.Shoot.performed -= OnShoot;
        }
    }
}
