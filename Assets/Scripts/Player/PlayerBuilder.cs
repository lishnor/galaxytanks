﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuilder : MonoBehaviour
{
    public TankBarrel barrel;
    public TankBody body;
    public TankTracks tracks;

    private SpriteRenderer barrelRenderer;
    private SpriteRenderer bodyRenderer;
    private SpriteRenderer tracksRenderer;

    void Start()
    {
        BuildTank();
    }

    [ContextMenu("BuildTank")]
    public void BuildTank() 
    {
        GetRenderers();

        BuildBarrel();
        BuildBody();
        BuildTracks();
    }

    void BuildBarrel() 
    {
        if (barrel != null)
        {
            barrelRenderer.transform.localPosition = barrel.offset;
            barrelRenderer.sprite = barrel.barrelSprite;
            barrelRenderer.sortingOrder = barrel.renderLayer;
        }
        else
            Debug.LogWarning("Could not build barrel");
    }

    void BuildBody()
    {
        if (body != null)
        {
            bodyRenderer.transform.localPosition = body.offset;
            bodyRenderer.sprite = body.bodySprite;
            bodyRenderer.sortingOrder = body.renderLayer;
        }
        else
            Debug.LogWarning("Could not build body");
    }

    void BuildTracks()
    {
        if (tracks != null)
        {
            tracksRenderer.transform.localPosition = tracks.offset;
            tracksRenderer.sprite = tracks.trackSprite;
            tracksRenderer.sortingOrder = tracks.renderLayer;
        }
        else
            Debug.LogWarning("Could not build tracks");
    }


    void GetRenderers()
    {
        if (barrelRenderer == null) barrelRenderer = transform.Find("Tank_barrel")?.GetComponent<SpriteRenderer>() ?? CreateRenderer("Tank_barrel");
        if (bodyRenderer == null) bodyRenderer = transform.Find("Tank_body")?.GetComponent<SpriteRenderer>() ?? CreateRenderer("Tank_body");
        if(tracksRenderer == null) tracksRenderer = transform.Find("Tank_tracks")?.GetComponent<SpriteRenderer>() ?? CreateRenderer("Tank_tracks");
    }

    SpriteRenderer CreateRenderer(string rendererName)
    {
        var renderer = new GameObject(rendererName).AddComponent<SpriteRenderer>();
        renderer.transform.parent = transform;
        return renderer;
    }
}
