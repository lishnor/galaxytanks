﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PartsManager : MonoBehaviour
{
    // [SerializeField] PlayerBuilder playerBuilder;

    [Header("BodyScriptableObjects")]
    public TankBody[] tankType;
    public TankTracks[] tankTracks;
    public TankBarrel[] tankBarrel;

    public void UpdateTankType(PlayerBuilder playerBuilder, uint type)
    {
        playerBuilder.body = (type >= tankType.Length) ? tankType.FirstOrDefault() : tankType[type];
        playerBuilder.tracks = (type >= tankTracks.Length) ? tankTracks.FirstOrDefault() : tankTracks[type];
        playerBuilder.barrel = (type >= tankBarrel.Length) ? tankBarrel.FirstOrDefault() : tankBarrel[type];
        playerBuilder.BuildTank();
    }
}
