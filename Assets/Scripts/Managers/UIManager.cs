﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    float startCurrentTime = 0f;
    float roundCurrentTime = 0f;
    float startingTime = 10f;
    float roundTime = 30f;
    bool roundStarted = false;

    [SerializeField]
    TextMeshProUGUI startCountdownTimer;
    [SerializeField]
    GameObject startCountdownPanel;
    [SerializeField]
    TextMeshProUGUI roundCountdownTimer;
    [SerializeField]
    TextMeshProUGUI coins;

    [SerializeField]
    List<HealthBar> healthBars;
    [SerializeField]
    List<TextMeshProUGUI> usernames;

    [SerializeField]
    CanvasGroup oscGroup;
    [SerializeField]
    CanvasGroup cursorGroup;
    [SerializeField]
    CanvasGroup myTurnGroup;
    [SerializeField]
    CanvasGroup victoryGroup;
    [SerializeField]
    CanvasGroup defeatGroup;
    [SerializeField]
    CanvasGroup everythingGroup;

    private void Start()
    {
        roundCountdownTimer.text = "";
        startCurrentTime = startingTime;
        roundCurrentTime = roundTime;
        BattleRoomManager.OnHealthChanged += OnHealthChanged;
        BattleRoomManager.OnNameChanged += OnNameChanged;
        BattleRoomManager.OnMyTurnStart += OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd += OnMyTurnEnd;
        BattleRoomManager.OnVictory += OnVictory;
        BattleRoomManager.OnDefeat += OnDefeat;
        BattleRoomManager.OnTimeChanged += OnTimeChanged;
        BattleRoomManager.OnGameStarted += OnGameStarted;
        everythingGroup.alpha = 0f;
        coins.text = ClientManager.CoinsAmount.ToString();
        ClientManager.OnCoinsAmountChanged += OnCoinsAmountChanged;
    }

    private void OnNameChanged(int playerNumber, string name)
    {
        if (playerNumber < usernames.Count)
        {
            usernames[playerNumber].text = name;
        }
    }

    private void OnCoinsAmountChanged(int coinsAmount)
    {
        coins.text = coinsAmount.ToString();
    }

    private void OnGameStarted()
    {
        everythingGroup.alpha = 1f;
    }

    private void OnMyTurnStart()
    {
        oscGroup.DOFade(1f, 1f);
        Sequence myTurnSequence = DOTween.Sequence();
        myTurnSequence.Append(myTurnGroup.DOFade(1f, 0.5f))
          .Append(myTurnGroup.DOFade(0f, 1f))
          .Append(cursorGroup.DOFade(1f, 1f));
        AudioManager.instance.Play("YourTurn");
    }

    private void OnMyTurnEnd()
    {
        oscGroup.DOFade(0.2f, 1f);
        cursorGroup.DOFade(0f, 1f);
        roundCountdownTimer.text = "";
    }

    private void OnVictory()
    {
        victoryGroup.DOFade(1f, 1f);
        cursorGroup.DOFade(0f, 1f);
        AudioManager.instance.Play("Winner");
    }

    private void OnDefeat()
    {
        defeatGroup.DOFade(1f, 1f);
        cursorGroup.DOFade(0f, 1f);
        AudioManager.instance.Play("Loser");
    }

    private void OnTimeChanged(float time)
    {
        roundCountdownTimer.text = time.ToString("0");
    }

    private void Update()
    {
        //StartCountdown();
        //if (roundStarted)
        //{
        //    RoundCountdown();
        //}
        
    }
    //public void RoundCountdown()
    //{
    //    roundCurrentTime -= 1 * Time.deltaTime;
    //    roundCountdownTimer.text = roundCurrentTime.ToString("0");
    //}
    //public void StartCountdown()
    //{
    //    if (startCurrentTime >= 0)
    //    {
    //        startCurrentTime -= 1 * Time.deltaTime;
    //        startCountdownTimer.text = startCurrentTime.ToString("0");
    //        if (startCurrentTime <= 0)
    //        {
    //            startCountdownPanel.SetActive(false);
    //            roundStarted = true;
    //        }
    //    }
        
    //}

    public void OnHealthChanged(int playerNumber, int health)
    {
        if(playerNumber < healthBars.Count)
        {
            healthBars[playerNumber].SetHealth(health);
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        BattleRoomManager.Leave?.Invoke();
    }

    void OnDestroy()
    {
        BattleRoomManager.OnHealthChanged -= OnHealthChanged;
        BattleRoomManager.OnNameChanged -= OnNameChanged;
        BattleRoomManager.OnMyTurnStart -= OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd -= OnMyTurnEnd;
        BattleRoomManager.OnVictory -= OnVictory;
        BattleRoomManager.OnDefeat -= OnDefeat;
        BattleRoomManager.OnTimeChanged -= OnTimeChanged;
        BattleRoomManager.OnGameStarted -= OnGameStarted;
        ClientManager.OnCoinsAmountChanged -= OnCoinsAmountChanged;
    }
}
