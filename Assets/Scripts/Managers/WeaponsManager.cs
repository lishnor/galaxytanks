﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

public class WeaponsManager : MonoBehaviour
{
    public PlayerShooting playerShootingSystem;

    [Header("UI")]
    [SerializeField] GameObject weaponSelectPanel;
    [SerializeField] TMP_Text ammoCounter;
    [SerializeField] Transform selectUIContainer;
    [SerializeField] WeaponSelectPanel selectPanel;
    [SerializeField] Image buttonImage;
    bool panelToggle = false;

    [Header("WeaponsScriptableObjects")]
    public Weapon[] weapons;

    private void Start()
    {
        InputManager.PlayerInput.Gameplay.WeaponSelectMenu.performed += ctx => ToggleWeaponSelectPanel();
        playerShootingSystem.shootAction += UpdateAmmoCounter;
        PopulateList();
        UpdateAmmoCounter();
    }

    public void PopulateList() 
    {
        foreach (var weapon in weapons)
        {
            var panel = (WeaponSelectPanel)Instantiate(selectPanel, selectUIContainer);
            panel.InitializeSelectPanel(weapon, this);
        }
    }

    public void SelectWeapon(Weapon weapon) 
    {
        if (ClientManager.CoinsAmount > weapon.price)
        {
            playerShootingSystem.weapon = weapon;
            playerShootingSystem.SetupWeapon();
            UpdateAmmoCounter();
            buttonImage.sprite = weapon.weaponSprite;
            ClientManager.AddRemoveCoins(-weapon.price);
        }
    }

    
    public void AddTarget(Component target) 
    {
        int i = 0;
        Component collider = null;

        while (collider != null || i == 0) 
        {
            collider = playerShootingSystem.shootingSystem.trigger.GetCollider(i);
            if (collider == target) break;
            else if (collider != null) i++;
        }
        playerShootingSystem.shootingSystem.trigger.SetCollider(i,target);
    }

    public void ToggleWeaponSelectPanel()
    {
        panelToggle = !panelToggle;
        weaponSelectPanel.SetActive(panelToggle);
    }

    [ContextMenu("UpdateAmmoCounter")]
    public void UpdateAmmoCounter() 
    {
        var text = (playerShootingSystem.AmmoAmount == -1)? "∞":playerShootingSystem.AmmoAmount.ToString();
        ammoCounter.text = text;
    }

    private void OnDestroy()
    {
        playerShootingSystem.shootAction -= UpdateAmmoCounter;
    }
}
