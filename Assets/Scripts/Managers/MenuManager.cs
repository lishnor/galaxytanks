﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI coins;
    [SerializeField]
    TextMeshProUGUI usernameDisp;

    [SerializeField]
    GameObject loginScreen;
    [SerializeField]
    GameObject homeScreen;
    [SerializeField]
    ClientManager clientManager;

    [SerializeField]
    TMP_InputField server;
    [SerializeField]
    TMP_InputField username;
    [SerializeField]
    TMP_InputField password;

    [SerializeField]
    GameObject loginFailed;


    void Start()
    {
        var token = PlayerPrefs.GetString("Token", string.Empty);
        if (token == string.Empty)
        {
            Analytics.CustomEvent("NewPlayer");
            loginScreen.SetActive(true);
            homeScreen.SetActive(false);
        } else
        {
            homeScreen.SetActive(true);
        }
        coins.text = ClientManager.CoinsAmount.ToString();
        usernameDisp.text = ClientManager.UserName;
        var serverAddress = PlayerPrefs.GetString("ServerAddress", "localhost:2567");
        server.text = serverAddress;

        ClientManager.OnCoinsAmountChanged += OnCoinsAmountChanged;
        ClientManager.OnUsernameChanged += OnUsernameChanged;
        ClientManager.OnLoggedIn += OnLoggedIn;
        ClientManager.OnLoginFailed += OnLoginFailed;
    }

    private void OnLoginFailed()
    {
        loginFailed.SetActive(true);
    }

    void OnCoinsAmountChanged(int coinsAmount)
    {
        coins.text = coinsAmount.ToString();
    }

    void OnUsernameChanged(string name)
    {
        usernameDisp.text = name;
    }

    void OnLoggedIn()
    {
        loginScreen.SetActive(false);
        homeScreen.SetActive(true);
    }
    
    public void Login()
    {
        // TODO: add validation
        clientManager.serverAddress = server.text;
        clientManager.Login(username.text, password.text);
        loginFailed.SetActive(false);
        Analytics.CustomEvent("UserLoggedin");
    }

    void OnDestroy()
    {
        ClientManager.OnCoinsAmountChanged -= OnCoinsAmountChanged;
        ClientManager.OnUsernameChanged -= OnUsernameChanged;
        ClientManager.OnLoggedIn -= OnLoggedIn;
        ClientManager.OnLoginFailed -= OnLoginFailed;
    }
}
