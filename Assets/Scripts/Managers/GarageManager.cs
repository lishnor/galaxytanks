﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class GarageManager : MonoBehaviour
{
    public PlayerShooting playerShootingSystem;

    [Header("UI")]
    //[SerializeField] GameObject weaponSelectPanel;
    [SerializeField] Transform selectUIContainer;
    [SerializeField] TankSelectPanel selectPanel;

    TankSelectPanel selectedTankSelectPanel;

    public Sprite[] tanks;

    private void Start()
    {
        PopulateList();
    }

    public void PopulateList() 
    {
        int selectedTank = PlayerPrefs.GetInt("TankType", 0);
        int i = 0;
        foreach (var tank in tanks)
        {
            var panel = (TankSelectPanel)Instantiate(selectPanel, selectUIContainer);
            panel.InitializeSelectPanel(i, tank, selectedTank == i, this);
            if(selectedTank == i)
            {
                selectedTankSelectPanel = panel;
            }
            i++;
        }
    }

    public void Select(TankSelectPanel panel)
    {
        if(panel != selectedTankSelectPanel)
        {
            selectedTankSelectPanel.Deselect();
            selectedTankSelectPanel = panel;
        }
    }
}
