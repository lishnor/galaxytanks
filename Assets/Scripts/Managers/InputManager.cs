﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static PlayerInputActions playerInput;
    public static PlayerInputActions PlayerInput => playerInput;

    private static InputManager instance;
    public static InputManager Instance => instance;

    private void Awake()
    {
        playerInput = new PlayerInputActions();

        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        playerInput.Gameplay.Enable();
    }
}
