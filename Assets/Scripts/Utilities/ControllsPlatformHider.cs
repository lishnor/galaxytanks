﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllsPlatformHider : MonoBehaviour
{
    [SerializeField] GameObject cLeft;
    [SerializeField] GameObject cRight;
    [SerializeField] GameObject cShoot;
    [SerializeField] GameObject cInfo;
    [SerializeField] RectTransform cSelectWeapon;

    private void Start()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        cLeft.SetActive(true);
        cRight.SetActive(true);
        cShoot.SetActive(true);
        cInfo.SetActive(false);

        cSelectWeapon.anchoredPosition = new Vector2(-450, 100);
#else
        cLeft.SetActive(false);
        cRight.SetActive(false);
        cShoot.SetActive(false);
         cInfo.SetActive(true);

        cSelectWeapon.anchoredPosition = new Vector2(-175, 100);
#endif
    }

}
