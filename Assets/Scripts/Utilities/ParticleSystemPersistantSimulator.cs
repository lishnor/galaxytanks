﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[ExecuteInEditMode]
public class ParticleSystemPersistantSimulator : MonoBehaviour
{
    [SerializeField] float simulationScalar = 0.5f;
    float simulationTime = 0;
    ParticleSystem system;

    void Awake()
    {
        system = GetComponent<ParticleSystem>();
        system.useAutoRandomSeed = false;
        system.randomSeed = 0;
        system.Simulate(0, false, false, true);
    }

    private void Update()
    {
        if (simulationTime >= system.main.duration)
        {
            simulationTime = 0;
        }

        simulationTime += Time.deltaTime;
        simulationTime *= simulationScalar;
        system.Simulate(simulationTime, false, false, true);
    }

}