﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemTrajectory : MonoBehaviour
{
    private ParticleSystem system;

    private void Awake()
    {
        system = GetComponent<ParticleSystem>();
        system.useAutoRandomSeed = false;
        system.randomSeed = 0;
    }

    public void Update() 
    {
        system.Simulate(system.main.startLifetimeMultiplier, false, true,true);
    }
}