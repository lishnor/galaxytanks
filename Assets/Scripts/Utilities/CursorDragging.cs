﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

public class CursorDragging : EventTrigger
{
    PlayerInputActions playerInput;
    Vector2 pointerPosition;
    Vector2 lastPointerPosition = new Vector2(0, 0);
    Vector2 draggingOffset;
    static bool highPrecision = false;
    bool _isEnabled = false;

    private void Start()
    {
        InputManager.PlayerInput.Gameplay.Pointer.performed += OnPointerPerformed;
        BattleRoomManager.OnMyTurnStart += OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd += OnMyTurnEnd;
    }

    void OnPointerPerformed(CallbackContext ctx)
    {
        pointerPosition = ctx.ReadValue<Vector2>();
    }

    private void OnMyTurnStart()
    {
        _isEnabled = true;
    }

    private void OnMyTurnEnd()
    {
        _isEnabled = false;
        dragging = false;
    }

    private bool dragging;

    public void Update()
    {
        if (dragging)
        {
            if (highPrecision)
            {
                Vector3 movement = pointerPosition - lastPointerPosition;
                transform.position += movement * 0.2f;
                lastPointerPosition = pointerPosition;
            } else {
                transform.position = pointerPosition - draggingOffset;
            }
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!_isEnabled) return;
        dragging = true;
        Vector2 pos = transform.position;
        draggingOffset = pointerPosition - pos;
        lastPointerPosition = transform.position;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
    }

    void OnDestroy()
    {
        BattleRoomManager.OnMyTurnStart -= OnMyTurnStart;
        BattleRoomManager.OnMyTurnEnd -= OnMyTurnEnd;
        InputManager.PlayerInput.Gameplay.Pointer.performed -= OnPointerPerformed;
    }
}
