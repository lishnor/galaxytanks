﻿using Colyseus;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class BattleRoomManager : MonoBehaviour
{
    public enum Target { Miss, Enemy, Self };
    private Room<RoomState> _room;
    private GameMap _gameMap;
    public int tankType { get; set; }

    public static UnityAction<GameMap> OnGameMapReceived;
    public static UnityAction<int, int, int> OnGameStarting;
    public static UnityAction OnMyTurnStart;
    public static UnityAction OnMyTurnEnd;

    // UI
    public static UnityAction OnGameStarted; // można wtedy odkryć scenę
    public static UnityAction<float> OnTimeChanged; // czas gracza upływa (sekundy)
    public static UnityAction<int, int> OnHealthChanged; // (0: lewy gracz, 1: prawy gracz; życie 1-100)
    public static UnityAction<int, string> OnNameChanged; // (0: lewy gracz, 1: prawy gracz,; nazwa gracza)
    public static UnityAction OnVictory;
    public static UnityAction OnDefeat;
    public static UnityAction Leave;

    EnemyController _enemyController;
    PlayerController _playerController;
    PlayerController _enemyPlayerController;

    const float roundTime = 20f;
    float timeLeft;
    bool myTurn = false;
    int playersPlanet;
    int winningPrize = 30;

    void Awake()
    {
        Leave += OnLeave;
    }

    public Room<RoomState> room
    {
        get { return _room; }
        set
        {
            _room = value;
            // _room.OnMessage<GameMap>("map", OnGameMap); // TODO: report Colyseus issue
            _room.OnMessage<StartMessage>("start", OnStart);
            _room.OnMessage<object>("yourTurn", OnMyTurn);
            _room.OnMessage<ShootMessage>("shoot", OnEnemyShoot);
            _room.OnMessage<HitMessage>("hit", OnEnemyHit);
            _room.OnMessage<GameOverMessage>("gameOver", OnGameOver);
            _room.State.players.OnChange += OnPlayersStateChange;
            _room.OnStateChange += OnStateChange;
        }
    }

    void OnStateChange(RoomState state, bool isFirstState)
    {
        if (isFirstState)
        {
            OnGameMap(state.gameMap);
        }
    }

    void OnGameMap(GameMap gameMap)
    {
        _gameMap = gameMap;
        if (SceneManager.GetActiveScene().name != Constants.battleSceneName)
        {
            SceneManager.LoadSceneAsync(Constants.battleSceneName, LoadSceneMode.Additive).completed += SceneReady;
            Debug.Log("Loaded scene");
        } else
        {
            SceneReady(null);
        }
    }

    void OnDestroy()
    {
        Leave -= OnLeave;
    }

    async void SceneReady(AsyncOperation a)
    {
        OnGameMapReceived?.Invoke(_gameMap);
        var data = new ReadyMessage
        {
            tankType = tankType
        };
        await _room.Send("ready", data);
        Debug.Log("OnGameMap");
    }
    
    void OnStart(StartMessage data)
    {
        // place player and opponent tanks in the scene, show the VS screen
        OnGameStarting?.Invoke(data.planetNumber, data.enemyTankType, tankType);
        
        _enemyController = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyController>();
        _playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _enemyPlayerController = GameObject.FindGameObjectWithTag("Enemy").GetComponent<PlayerController>();
        playersPlanet = data.planetNumber;
        _playerController.OnShoot += PlayerShoot;
        _playerController.OnMove += PlayerMove;
        _playerController.OnGetHit += (int damage) => PlayerHit(Target.Self, damage, 0);
        _enemyPlayerController.OnGetHit += (int damage) => PlayerHit(Target.Enemy, damage, 0);
        _playerController.OnShootingEnd += PlayerShootingEnd;
        OnNameChanged?.Invoke(playersPlanet, ClientManager.UserName);
        OnNameChanged?.Invoke(1-playersPlanet, data.enemyName);
        Debug.Log(ClientManager.UserName);
        Debug.Log(data.enemyName);
        if (SceneManager.GetActiveScene().name != Constants.battleSceneName)
        {
            Debug.Log("Unload");
            SceneManager.UnloadSceneAsync(Constants.menuSceneName).completed += (AsyncOperation a) => OnGameStarted?.Invoke();
        } else
        {
            OnGameStarted?.Invoke();
        }
        Debug.Log("OnStart");
    }

    void Update()
    {
        if (myTurn)
        {
            timeLeft = Mathf.Max(timeLeft - Time.deltaTime, 0f);
            OnTimeChanged?.Invoke(timeLeft);
            if (timeLeft == 0f)
            {
                EndTurn(true);
            }
        }
    }

    void OnMyTurn(object nothing)
    {
        // start the countdown
        // let the player move and shoot
        timeLeft = roundTime;
        myTurn = true;
        OnMyTurnStart?.Invoke();
        Debug.Log("OnMyTurn");
    }

    void OnEnemyShoot(ShootMessage data)
    {
        // shoot the bullet
        _enemyController.Shoot(data.tankAngle, data.gunAngle, data.weaponType);
        Debug.Log("OnEnemyShoot");
    }

    void OnEnemyHit(HitMessage data)
    {
        Debug.Log("OnEnemyHit");
    }

    void OnPlayersStateChange(Player player, string id)
    {
        if (id != _room.SessionId)
        {
            _enemyController.SetGunAngle(player.gunAngle);
            _enemyController.SetTankAngle(player.tankAngle);
            OnHealthChanged?.Invoke(1 - playersPlanet, player.health);
            Debug.Log($"OnPlayersStateChange {player.gunAngle} {player.tankAngle} {player.health}");
        } else
        {
            //_playerController.SetHealth(player.health);
            OnHealthChanged?.Invoke(playersPlanet, player.health);
            Debug.Log($"OnPlayersStateChange {player.health}");
        }
    }

    void OnGameOver(GameOverMessage data)
    {
        if (data.winnerId == _room.SessionId)
        {
            OnVictory?.Invoke();
            ClientManager.AddRemoveCoins(winningPrize);
        } else
        {
            OnDefeat?.Invoke();
        }
        Debug.Log("OnGameOver");
    }

    public async void PlayerMove(float tankAngle, float gunAngle)
    {
        var data = new MoveMessage
        {
            tankAngle = tankAngle,
            gunAngle = gunAngle
        };
        await _room.Send("move", data);
        Debug.Log("PlayerMove");
    }
    
    public async void PlayerShoot(float tankAngle, float gunAngle, int weaponType)
    {
        var data = new ShootMessage
        {
            tankAngle = tankAngle,
            gunAngle = gunAngle,
            weaponType = weaponType
        };
        EndTurn();
        await _room.Send("shoot", data);
        Debug.Log("PlayerShoot");
    }

    public async void PlayerHit(Target target, float damageMultiplier, int weaponType)
    {
        var data = new HitMessage
        {
            target = (int)target,
            weaponType = weaponType,
            damageMultiplier = damageMultiplier
        };
        await _room.Send("hit", data);
        Debug.Log("PlayerHit");
    }

    public async void PlayerShootingEnd()
    {
        await _room.Send("shootingEnd");
        Debug.Log("PlayerShootingEnd");
    }

    async void EndTurn(bool timeout = false)
    {
        myTurn = false;
        OnMyTurnEnd?.Invoke();
        if (timeout)
        {
            await _room.Send("timeout");
            Debug.Log("Timeout");
        }
        Debug.Log("My turn ended");
    }
    
    async void OnLeave()
    {
        await _room.Leave(true);
        Destroy(this);
        Debug.Log("Leave");
    }
}
