﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    PlayerMovement _movement;
    PlayerShooting _shooting;

    void Awake()
    {
        _movement = GetComponent<PlayerMovement>();
        _shooting = GetComponent<PlayerShooting>();
    }

    public void SetGunAngle(float gunAngle)
    {
        _shooting.gunAngle = gunAngle;
    }

    public void SetTankAngle(float tankAngle)
    {
        _movement.angle = tankAngle;
    }

    public void Shoot(float tankAngle, float gunAngle, int weaponType)
    {
        // TODO: use setters with addtional calls for immediate change or defer shooting to update
        _shooting.gunAngle = gunAngle;
        _movement.angle = tankAngle;
        _shooting.Shoot();
    }
}
