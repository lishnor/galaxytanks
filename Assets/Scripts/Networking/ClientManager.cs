﻿using Colyseus;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ClientManager : MonoBehaviour
{
    private static ClientManager _instance = null;
    private BattleRoomManager _battleRoomManager;
    private Client _client;
    private int _tankType;

    private int _coinsAmount;
    private string _userName;

    public static int CoinsAmount => _instance._coinsAmount;
    public static string UserName => _instance._userName;

    public static UnityAction<string> OnJoinFailed;
    public static UnityAction<int> OnCoinsAmountChanged;
    public static UnityAction<string> OnUsernameChanged;
    public static UnityAction OnLoggedIn;
    public static UnityAction OnLoginFailed;

    private const string _battleRoomName = "battle";
    private const string _lobbyRoomName = "lobby";

    private int initialCoinsAmount = 200;
    private string savePath;

    private string _serverAddress;
    public string serverAddress
    {
        get { return _serverAddress; }
        set {
            _serverAddress = value;
            _client = new Client("ws://" + _serverAddress);
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            // to nie jest dobra praktyka, ale rozne metody sa podpiete w UI wiec przy zmianie sceny trzeba przelaczyc sie na nowa instancje
            _battleRoomManager = _instance._battleRoomManager;
            _coinsAmount = _instance._coinsAmount;
            _userName = _instance._userName;
            Destroy(_instance);
        }
        _instance = this;
        savePath = Application.persistentDataPath + "/save.dat";
        Load();
        serverAddress = PlayerPrefs.GetString("ServerAddress", "localhost:2567");
        DontDestroyOnLoad(gameObject);
    }

    void OnDestroy()
    {
        Debug.Log("Destroying client");
    }

    async void Start()
    {
        var token = PlayerPrefs.GetString("Token", string.Empty);
        if (token != string.Empty)
        {
            await _client.Auth.Login();
            SetUserData();
        }

    }

    public async void Login(string email, string password)
    {
        try
        {
            await _client.Auth.Login(email, password);
            SetUserData();
            OnLoggedIn?.Invoke();
        } catch (Exception)
        {
            OnLoginFailed?.Invoke();
        }
    }

    void SetUserData()
    {
        _userName = _client.Auth.Email;
        OnUsernameChanged?.Invoke(_client.Auth.Email);
        Debug.Log(_client.Auth.Email);
    }

    public async void JoinRandomRoom()
    {
        Debug.Log("JoinRandomRoom");
        try
        {
            Debug.Log(_userName);
            var room = await _client.JoinOrCreate<RoomState>(_battleRoomName, new Dictionary<string, object> { { "name", _userName } });
            RoomJoined(room);
        }
        catch (Exception ex)
        {
            JoinFailed(ex);
        }
    }

    public async void JoinRoomById(string roomId)
    {
        Debug.Log("JoinRoomById");
        try
        {
            var room = await _client.JoinById<RoomState>(roomId);
            RoomJoined(room);
        }
        catch (Exception ex)
        {
            JoinFailed(ex);
        }
    }

    public async void CreateRoom()
    {
        Debug.Log("CreateRoom");
        try
        {
            var room = await _client.Create<RoomState>(_battleRoomName, new Dictionary<string, object> { { "private", true } });
            RoomJoined(room);
        }
        catch (Exception ex)
        {
            JoinFailed(ex);
        }
    }

    void JoinFailed(Exception ex)
    {
        Debug.Log("Join failed: " + ex.Message);
        OnJoinFailed?.Invoke(ex.Message);
    }

    void RoomJoined(Room<RoomState> room)
    {
        Debug.Log("RoomJoined");
        _battleRoomManager = gameObject.AddComponent<BattleRoomManager>() as BattleRoomManager;
        _battleRoomManager.room = room;
        _battleRoomManager.tankType = PlayerPrefs.GetInt("TankType", 0);
    }

    public static void AddRemoveCoins(int amount)
    {
        _instance.AddRemoveCoinsInternal(amount);
    }
    
    public void AddRemoveCoinsInternal(int amount)
    {
        _coinsAmount += amount;
        OnCoinsAmountChanged.Invoke(_coinsAmount);
        Save();
    }

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(savePath, FileMode.OpenOrCreate);
        SaveData saveData = new SaveData
        {
            coinsAmount = _coinsAmount
        };
        bf.Serialize(file, saveData);
        file.Close();
    }

    void Load()
    {
        if (File.Exists(savePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.Open);
            SaveData saveData = (SaveData)bf.Deserialize(file);
            file.Close();
            _coinsAmount = saveData.coinsAmount;
        }
        else
        {
            _coinsAmount = initialCoinsAmount;
        }
        OnCoinsAmountChanged?.Invoke(_coinsAmount);
    }

    public void Logout()
    {
        _coinsAmount = initialCoinsAmount;
        PlayerPrefs.SetString("Token", string.Empty);
    }
}

[Serializable]
public class SaveData
{
    public int coinsAmount;
}
