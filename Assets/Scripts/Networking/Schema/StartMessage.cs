// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class StartMessage : Schema {
	[Type(0, "int32")]
	public int planetNumber = 0;

	[Type(1, "int32")]
	public int enemyTankType = 0;

	[Type(2, "string")]
	public string enemyName = "";
}

