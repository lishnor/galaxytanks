// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class HitMessage : Schema {
	[Type(0, "int32")]
	public int target = 0;

	[Type(1, "int32")]
	public int weaponType = 0;

	[Type(2, "number")]
	public float damageMultiplier = 0;
}

