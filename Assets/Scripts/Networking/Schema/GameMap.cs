// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class GameMap : Schema {
	[Type(0, "number")]
	public float firstPlayerAngle = 0;

	[Type(1, "number")]
	public float secondPlayerAngle = 0;

	[Type(2, "array", typeof(ArraySchema<GameMapObject>))]
	public ArraySchema<GameMapObject> objects = new ArraySchema<GameMapObject>();
}

