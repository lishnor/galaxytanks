// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class GameMapObject : Schema {
	[Type(0, "int32")]
	public int type = 0;

	[Type(1, "int32")]
	public int variant = 0;

	[Type(2, "number")]
	public float radius = 0;

	[Type(3, "number")]
	public float rotation = 0;

	[Type(4, "number")]
	public float x = 0;

	[Type(5, "number")]
	public float y = 0;
}

