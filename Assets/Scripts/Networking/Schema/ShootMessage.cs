// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.5.39
// 

using Colyseus.Schema;

public class ShootMessage : Schema {
	[Type(0, "number")]
	public float tankAngle = 0;

	[Type(1, "number")]
	public float gunAngle = 0;

	[Type(2, "int32")]
	public int weaponType = 0;
}

