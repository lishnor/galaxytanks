﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Planet : MonoBehaviour
{
    public uint variant= 0;
    public float radius = 1f;
    public float rotation;
    public bool flipX;
    public bool flipY;
    public Vector2 worldPosition;
    [SerializeField] Sprite[] spriteVariants;
    [SerializeField] ParticleSystemForceField gravityField;
    Vector3 standardScale = Vector3.one;

    [ContextMenu("SetPlanet")]
    public void UsePlanetParameters() 
    {
        var spriteRender = GetComponent<SpriteRenderer>();

        var gravity = gravityField.gravity;
        gravity.curve.keys[0].value = 3f+radius/2f;
        gravityField.gravity = gravity;

        spriteRender.sprite = (variant >= spriteVariants.Length) ? spriteVariants.FirstOrDefault() : spriteVariants[variant];
        spriteRender.flipX = flipX;
        spriteRender.flipY = flipY;

        transform.localScale = new Vector3(standardScale.x * radius, standardScale.y * radius, standardScale.z);
        transform.rotation = Quaternion.Euler(0, 0, rotation);
        transform.position = worldPosition;
    }
}
