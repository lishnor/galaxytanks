﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class GameMapBuilder : MonoBehaviour
{
    [SerializeField] Planet planetToInstantiate;
    [SerializeField] Transform planetsParent;
    [SerializeField] PartsManager partsManager;

    GameMap gameMap;

    List<Planet> createdPlanets = new List<Planet>();

    void Awake()
    {
        BattleRoomManager.OnGameMapReceived += OnGameMapReceived;
        BattleRoomManager.OnGameStarting += OnGameStarting;
    }

    void OnGameMapReceived(GameMap gameMap)
    {
        Debug.Log("GameMapBuilder: OnGameMapReceived");
        for (int i = 0; i < gameMap.objects.Count; ++i)
        {
            ProcessObject(i, gameMap.objects[i]);
        }
        this.gameMap = gameMap;
        Analytics.CustomEvent("SizeOfMap",new Dictionary<string, object> { { "NumberOfObjectsInMap",gameMap.objects.Count} });
    }

    void ProcessObject(int i, GameMapObject obj)
    {
        switch (obj.type)
        {
            case 0: // planet
                CreatePlanet(obj);
                break;
            case 1: // asteroid belt
                // CreateAsteroidBelt(obj);
                break;
        }
    }

    public void CreatePlanet(GameMapObject obj)
    {
        Debug.Log("Creating a planet");
        var planet = Instantiate(planetToInstantiate, planetsParent);
        planet.radius = obj.radius;
        planet.rotation = obj.rotation;
        planet.variant = (uint)obj.variant;
        planet.worldPosition = new Vector2(obj.x, obj.y);
        planet.UsePlanetParameters();
        createdPlanets.Add(planet);
    }

    void OnGameStarting(int playerPlanetNumber, int enemyTankType, int myTankType)
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        var movement = player.GetComponent<PlayerMovement>();
        movement.angle = playerPlanetNumber == 0 ? gameMap.firstPlayerAngle : gameMap.secondPlayerAngle;
        movement.planet = createdPlanets[playerPlanetNumber];
        var builder = player.GetComponent<PlayerBuilder>();
        partsManager.UpdateTankType(builder, (uint)myTankType);
        movement.PlaceOnPlanet();
        player.SetActive(true);
        var enemy = GameObject.FindGameObjectWithTag("Enemy");
        movement = enemy.GetComponent<PlayerMovement>();
        movement.angle = playerPlanetNumber == 1 ? gameMap.firstPlayerAngle : gameMap.secondPlayerAngle;
        movement.planet = createdPlanets[1-playerPlanetNumber];
        builder = enemy.GetComponent<PlayerBuilder>();
        partsManager.UpdateTankType(builder, (uint)enemyTankType);
        movement.PlaceOnPlanet(); // TODO: separate placement from controls
        enemy.SetActive(true);

        var shooting1 = player.GetComponent<PlayerShooting>();
        var shooting2 = enemy.GetComponent<PlayerShooting>();
        int i = 3;
        foreach (var planet in createdPlanets) 
        {
            shooting1.shootingSystem.trigger.SetCollider(i, planet.GetComponent<CircleCollider2D>());
            shooting2.shootingSystem.trigger.SetCollider(i, planet.GetComponent<CircleCollider2D>());
            i++;
        }
    }

    void OnDestroy()
    {
        BattleRoomManager.OnGameMapReceived -= OnGameMapReceived;
        BattleRoomManager.OnGameStarting -= OnGameStarting;
    }

}
