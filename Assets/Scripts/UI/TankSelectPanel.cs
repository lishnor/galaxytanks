﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TankSelectPanel : MonoBehaviour
{

    [SerializeField] Image tankImage;
    [SerializeField] Button selectButton;
    [SerializeField] CanvasGroup tankImageGroup;
    [SerializeField] CanvasGroup buttonGroup;
    GarageManager manager;
    int tankType;

    public void InitializeSelectPanel(int tankType, Sprite tankSprite, bool selected, GarageManager manager) 
    {
        this.tankType = tankType;
        this.manager = manager;
        tankImage.sprite = tankSprite;
        selectButton.onClick.AddListener(Select);
        if(selected)
        {
            tankImageGroup.alpha = 1f;
            buttonGroup.alpha = 0.3f;
        } else
        {
            Deselect();
        }
    }

    void Select()
    {
        PlayerPrefs.SetInt("TankType", tankType);
        manager.Select(this);
        tankImageGroup.alpha = 1f;
        buttonGroup.alpha = 0.3f;
    }

    public void Deselect()
    {
        tankImageGroup.alpha = 0.3f;
        buttonGroup.alpha = 1f;
    }
}
