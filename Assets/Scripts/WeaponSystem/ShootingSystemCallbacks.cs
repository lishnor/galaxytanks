﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(ParticleSystem))]
public class ShootingSystemCallbacks : MonoBehaviour
{
    ParticleSystem ps;

    List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();

    public Action<Vector3> AnyParticleHit;
    public Action ParticlesDied;
    int lastParticleCount;

    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleTrigger()
    {
        int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        

        for (int i = 0; i < numEnter; i++)
        {
            AnyParticleHit?.Invoke(enter[i].position);
        }
        if (ps.particleCount == 0 && lastParticleCount > 0)
        {
            Debug.Log("Particles dead");
            ParticlesDied?.Invoke();
        }
        if (lastParticleCount > ps.particleCount)
        {
            AudioManager.instance.Play("Explosion");
        }
        lastParticleCount = ps.particleCount;
    }

}
