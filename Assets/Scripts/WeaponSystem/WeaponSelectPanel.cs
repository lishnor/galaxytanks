﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponSelectPanel : MonoBehaviour
{

    [SerializeField] Image weaponImage;
    [SerializeField] TMP_Text weaponName;
    [SerializeField] TMP_Text weaponStats;
    [SerializeField] TMP_Text ammoAmount;
    [SerializeField] TMP_Text weaponPrice;
    [SerializeField] Button buyButton;

    public void InitializeSelectPanel(Weapon weapon, WeaponsManager weaponsManager) 
    {
        weaponImage.sprite = weapon.weaponSprite;
        weaponImage.SetNativeSize();
        weaponImage.rectTransform.sizeDelta = weaponImage.rectTransform.sizeDelta * 2.5f;
        weaponImage.rectTransform.rotation = Quaternion.Euler(0, 0, 45f);
        weaponName.text = weapon.name;

        var stats = "<b>Stats:</b>\n";
        stats += $"damage: <b>{weapon.damage}</b>\t";
        stats += $"range: <b>{weapon.speed*weapon.lifeTime}</b>\n";
        stats += $"cycles: <b>{weapon.cycles}</b>\t";
        stats += $"boom size: <b>{weapon.rangeOfPerfectHit}</b>\n";
        weaponStats.text = stats;

        ammoAmount.text = (weapon.amount == -1) ? "∞" : weapon.amount.ToString();
        weaponPrice.text = $"{weapon.price}";

        buyButton.onClick.AddListener(() => weaponsManager.SelectWeapon(weapon));
    }
}
