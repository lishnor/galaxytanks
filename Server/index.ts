import http from "http";
import express from "express";
import cors from "cors";
import { Server } from "colyseus";
import { monitor } from "@colyseus/monitor";
import socialRoutes from "@colyseus/social/express"


import { BattleRoom } from "./src/BattleRoom";
import { GameMapGenerator } from "./src/GameMapGenerator";
import "./src/Auth";

const port = Number(process.env.PORT || 2567);
const app = express()

app.use(cors());
app.use(express.json())

const server = http.createServer(app);
const gameServer = new Server({
  server,
});

gameServer.define('battle', BattleRoom);

app.use("/colyseus", monitor());

app.get('/generatorTest', function (req, res) {
  res.send(new GameMapGenerator().generate())
})

app.use("/", socialRoutes);

gameServer.listen(port);
console.log(`Listening on ws://localhost:${ port }`)
