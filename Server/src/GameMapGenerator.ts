import { PlanetTemplates, MapObjectTemplates, AsteroidBeltTemplates } from "./GameMapTemplates"
import { Schema, ArraySchema, type } from "@colyseus/schema"
import _ from "lodash"

export class GameMapObject extends Schema {
	@type("int32")
	type: number

	@type("int32")
	variant: number

	@type("number")
	radius: number
	
	@type("number")
	rotation: number

	@type("number")
	x: number

	@type("number")
	y: number
}

export class GameMap extends Schema {
	@type("number")
	firstPlayerAngle: number

	@type("number")
	secondPlayerAngle: number

	@type([ GameMapObject ])
    objects = new ArraySchema<GameMapObject>()
    // @type({ map: GameMapObject })
    // objects = new MapSchema<GameMapObject>();
}

export class GameMapGenerator {
	distanceRange = {
		x: { min: 30, max: 45 },
		y: { min: 0, max: 10 }
	}
	extraObjectsNumberRange = { min: 1, max: 2 }
	extraObjectsRadiusRange = { min: 2, max: 4 }
	playerAngleRange = {
		first: { min: -0.8, max: -0.2},
		second: { min: 0.2, max: 0.8}
	}
	planetMargin = 4
	scale = 0.3

	generate(): GameMap {
		console.log("Generator start")
		const gameMap = new GameMap()
		let availablePlanets = _.keys(PlanetTemplates)
		const { value: firstPlanetVariant, arrayAfter: planetsAfterFirst } = randomFromArray(availablePlanets)
		const { value: secondPlanetVariant, arrayAfter: planetsAfterSecond } = randomFromArray(planetsAfterFirst)
		const playerPlanetsDistanceX = _.random(this.distanceRange.x.min, this.distanceRange.x.max, true)
		const playerPlanetsDistanceY = _.random(this.distanceRange.y.min, this.distanceRange.y.max, true) * (_.random(0, 1) === 1 ? 1 : -1)
		const firstPlanet = generateMapObject(PlanetTemplates, firstPlanetVariant, -playerPlanetsDistanceX/2.0, -playerPlanetsDistanceY/2.0)
		const secondPlanet = generateMapObject(PlanetTemplates, secondPlanetVariant, playerPlanetsDistanceX/2.0, playerPlanetsDistanceY/2.0)
		firstPlanet.type = 0
		secondPlanet.type = 0
		gameMap.objects.push(firstPlanet) // gameMap.objects['0'] = firstPlanet
		gameMap.objects.push(secondPlanet) // gameMap.objects['1'] = secondPlanet
		// gameMap.firstPlayerAngle = _.random(this.playerAngleRange.first.min, this.playerAngleRange.first.max, true)
		// gameMap.secondPlayerAngle = _.random(this.playerAngleRange.second.min, this.playerAngleRange.second.max, true)
		gameMap.firstPlayerAngle = 0;
		gameMap.secondPlayerAngle = 0;

		const extraObjectsNumber = _.random(this.extraObjectsNumberRange.min, this.extraObjectsNumberRange.max)
		availablePlanets = planetsAfterSecond
		let availableAsteroidBelts = _.keys(AsteroidBeltTemplates)

		const extraObjects: GameMapObject[] = []
		const extraObjectsSpace = {
			x: { min: -playerPlanetsDistanceX/2.0+firstPlanet.radius+this.planetMargin, max: playerPlanetsDistanceX/2.0-secondPlanet.radius-this.planetMargin },
			y: { min: -this.distanceRange.y.max/2.0+4.0, max: this.distanceRange.y.max/2.0+4.0 }
		}
		for (let i = 0; i < extraObjectsNumber; ++i) {
			let x: number
			let y: number
			let radius: number
			let fits: boolean
			let attempts = 0
			do {
				radius = _.random(this.extraObjectsRadiusRange.min, this.extraObjectsRadiusRange.max, true)
				x = _.random(extraObjectsSpace.x.min+radius, extraObjectsSpace.x.max-radius, true)
				y = _.random(extraObjectsSpace.y.min+radius, extraObjectsSpace.y.max-radius, true)
				fits = true
				for (let j = 0; j < extraObjects.length; ++j) {
					const a = { x, y, radius}
					const b = { x: extraObjects[j].x, y: extraObjects[j].y, radius: extraObjects[j].radius }
					if (overlaps(a, b, this.planetMargin)) {
						console.log("Overlap detected")
						fits = false
						++attempts;
						break
					}
				}
			} while (!fits && attempts < 6)
			if (attempts >= 6)
			{
				continue;
			}

			const isPlanet = true //_.random(0, 1) === 1
			if (isPlanet && availablePlanets.length > 0) {
				const { value: variant, arrayAfter: planetsLeft } = randomFromArray(availablePlanets)
				availablePlanets = planetsLeft
				const planet = generateMapObject(PlanetTemplates, variant, x, y, radius)
				planet.type = 0
				extraObjects.push(planet) // gameMap.objects[(i+2).toString()] = planet
			} else if (availableAsteroidBelts.length > 0) {
				const { value: variant, arrayAfter: beltsLeft } = randomFromArray(availableAsteroidBelts)
				availableAsteroidBelts = beltsLeft
				const belt = generateMapObject(AsteroidBeltTemplates, variant, x, y, radius)
				belt.type = 1
				extraObjects.push(belt) // gameMap.objects[(i+2).toString()] = belt
			}
		}
		gameMap.objects.push(...extraObjects)
		if (this.scale != 1.0) {
			gameMap.objects.forEach(o=>{
				o.x *= this.scale
				o.y *= this.scale
				o.radius *= this.scale
			})
		}
		console.log("Generator stop")
		return gameMap
	}
}

function randomFromArray(array: any[]): { value: any, arrayAfter: any[] } {
	const index = _.random(0, array.length-1)
	const arrayAfter = [...array]
	arrayAfter.splice(index, 1)
	return { value: parseInt(array[index], 10), arrayAfter }
}

function generateMapObject(templates: MapObjectTemplates, variant: number, x: number, y: number, radiusOverride?: number): GameMapObject {
	const template = templates[variant]
	const mapObject = new GameMapObject()
	mapObject.variant = variant
	mapObject.radius = radiusOverride || _.random(template.radius.min, template.radius.max, true)
	mapObject.rotation = _.random(template.rotation.min, template.rotation.max, true)
	mapObject.x = x
	mapObject.y = y
	return mapObject
}

interface Circle {
	x: number
	y: number
	radius: number
}

function overlaps(a: Circle, b: Circle, margin: number) {
	console.log(`Overlap check ${a.x} ${a.y} ${a.radius}, ${b.x} ${b.y} ${b.radius}`)
	return Math.sqrt(Math.pow(a.x-b.x, 2)+Math.pow(a.y-b.y, 2)) <= a.radius+b.radius+margin
}
