import { Schema, type } from "@colyseus/schema"

export class MoveMessage extends Schema {
	@type("number")
	tankAngle: number

	@type("number")
	gunAngle: number
}

export class ShootMessage extends Schema {
	@type("number")
	tankAngle: number

	@type("number")
	gunAngle: number

	@type("int32")
	weaponType: number
}

export class HitMessage extends Schema {
	@type("int32")
	target: number // 0 - miss, 1 - enemy, 2 - self
	
	@type("int32")
	weaponType: number

	@type("number")
	damageMultiplier: number
}

export class ChangeWeaponMessage extends Schema {
	@type("int32")
	weaponType: number
}

export class GameOverMessage extends Schema {
	@type("string")
	winnerId: string
}

export class ReadyMessage extends Schema {
	@type("int32")
	tankType: number
}

export class StartMessage extends Schema {
	@type("int32")
	planetNumber: number

	@type("int32")
	enemyTankType: number

	@type("string")
	enemyName: string
}
