import { Room, Client } from "colyseus"
import { RoomState, Player } from "./RoomState"
import { GameMap, GameMapGenerator } from "./GameMapGenerator"
import _ from "lodash"
import { MoveMessage, ShootMessage, HitMessage, ChangeWeaponMessage, GameOverMessage, ReadyMessage, StartMessage } from "./messages"
import { Weapons } from "./Weapons"

export class BattleRoom extends Room<RoomState> {

  roomSize: number = 2
  gameMap: GameMap
  playersReady: Set<string>
  whoseTurn: number
  clientsById: Map<string, Client> = new Map<string, Client>()
  tankTypes: Map<string, number> = new Map<string, number>()
  playerNames: Map<string, string> = new Map<string, string>()
  playerActive: string
  playerWaiting: string
  gameIsOver: boolean = false
  fakeOpponentFORTESTINGONLY: boolean = false

  onCreate (options: any) {
    this.maxClients = this.roomSize
    if (options.private) {
      this.setPrivate(true)
    }
    this.onMessage("ready", this.onPlayerReady.bind(this))
    this.onMessage("move", this.onPlayerMove.bind(this))
    this.onMessage("shoot", this.onPlayerShoot.bind(this))
    this.onMessage("timeout", this.onPlayerTimeout.bind(this))
    this.onMessage("shootingEnd", this.onPlayerShootingEnd.bind(this))
    this.onMessage("hit", this.onPlayerHit.bind(this))
    this.onMessage("changeWeapon", this.onPlayerChangedWeapon.bind(this))
    this.init()
  }

  init() {
    this.setState(new RoomState())
    this.gameMap = new GameMapGenerator().generate()
    this.state.gameMap = this.gameMap
    this.playersReady = new Set<string>()
    this.whoseTurn = this.fakeOpponentFORTESTINGONLY ? 0 : _.random(0, 1)
  }

  onJoin (client: Client, options: any) {
    const player = new Player()
    player.health = 100
    player.gunAngle = 0.5
    player.weaponType = 0
    this.playerNames.set(client.sessionId, options.name)
    this.state.players[client.sessionId] = player
    this.clientsById.set(client.sessionId, client)
    // client.send("map", this.gameMap) // TODO: Report Colyseus issue
    if (this.hasReachedMaxClients()) {
      this.lock()
    }
    console.log(`Client ${client.sessionId} joined room ${this.roomId}`);
  }

  onLeave (client: Client, consented: boolean) {
    if (!this.gameIsOver) {
      if (this.playerActive === client.sessionId) {
        this.gameOver(this.playerWaiting)
      } else {
        this.gameOver(this.playerActive)
      }
    }
    console.log(`Client ${client.sessionId} left room ${this.roomId}`);
  }

  onDispose() {
  }

  onPlayerReady(client: Client, data: ReadyMessage) {
    this.playersReady.add(client.sessionId)
    this.tankTypes.set(client.sessionId, data.tankType)
    if (this.fakeOpponentFORTESTINGONLY) {
      this.playersReady.add('fake')
      this.tankTypes.set('fake', 1)
      const player = new Player()
      player.health = 100
      player.gunAngle = 0.5
      player.weaponType = 0
      this.state.players['fake'] = player
    }
    if (this.playersReady.size == 2) {
      this.start()
    }
    console.log(`Client ${client.sessionId} is ready`);
  }

  onPlayerMove(client: Client, data: MoveMessage) {
    const player = this.getPlayerById(client.sessionId)
    player.tankAngle = data.tankAngle
    player.gunAngle = data.gunAngle
  }

  onPlayerShoot(client: Client, data: ShootMessage) {
    if (client.sessionId !== this.playerActive) return
    this.broadcast("shoot", data, { except: client })
    console.log(`Client ${client.sessionId} shot`);
  }

  onPlayerHit(client: Client, data: HitMessage) {
    if (client.sessionId !== this.playerActive) return
    console.log(`Client ${client.sessionId} hit`);
    this.broadcast("hit", data, { except: client })
    if(data.target !== 0) {
      const player = this.getPlayerById(data.target == 1 ? this.playerWaiting : this.playerActive)
      player.health -= data.damageMultiplier //*Weapons[data.weaponType].damage
      if (player.health <= 0) {
        player.health = 0
        this.gameOver(client.sessionId)
        return
      }
    }
  }

  onPlayerTimeout(client: Client, data: any) {
    if (client.sessionId !== this.playerActive) return
    console.log(`Client ${client.sessionId} timed out`);
    if(!this.gameIsOver) {
      this.nextRound()
    }
  }

  onPlayerShootingEnd(client: Client, data: any) {
    if (client.sessionId !== this.playerActive) return
    console.log(`Client ${client.sessionId} ended shooting`);
    if(!this.gameIsOver) {
      this.nextRound()
    }
  }

  onPlayerChangedWeapon(client: Client, data: ChangeWeaponMessage) {
    const player = this.getPlayerById(client.sessionId)
    player.weaponType = data.weaponType
    console.log(`Client ${client.sessionId} changed weapon`);
  }

  start() {
    this.playerActive = _.keys(this.state.players)[this.whoseTurn]
    this.playerWaiting = _.keys(this.state.players)[1-this.whoseTurn]
    const dataActive = new StartMessage()
    dataActive.planetNumber = 0
    dataActive.enemyTankType = this.tankTypes.get(this.playerWaiting)
    dataActive.enemyName = this.playerNames.get(this.playerWaiting)
    const dataWaiting = new StartMessage()
    dataWaiting.planetNumber = 1
    dataWaiting.enemyTankType = this.tankTypes.get(this.playerActive)
    dataWaiting.enemyName = this.playerNames.get(this.playerActive)
    this.getPlayerById(this.playerActive).tankAngle = this.gameMap.firstPlayerAngle
    this.getPlayerById(this.playerWaiting).tankAngle = this.gameMap.secondPlayerAngle
    this.clientsById.get(this.playerActive)?.send("start", dataActive)
    this.clientsById.get(this.playerWaiting)?.send("start", dataWaiting)
    this.nextRound(true)
    console.log(`Game starting in room ${this.roomId}`);
  }

  nextRound(isFirst = false) {
    this.clock.setTimeout(() => {
      if (!isFirst) {
        this.whoseTurn = 1-this.whoseTurn
        this.playerActive = _.keys(this.state.players)[this.whoseTurn]
        this.playerWaiting = _.keys(this.state.players)[1-this.whoseTurn]
      }
      this.clientsById.get(this.playerActive)?.send("yourTurn", {})
      console.log(`Round starting in room ${this.roomId}`);
    }, 2000);
  }

  gameOver(winnerId: string) {
    const data = new GameOverMessage()
    data.winnerId = winnerId
    this.gameIsOver = true
    this.broadcast("gameOver", data)
    console.log(`Game over in room ${this.roomId}`);
  }

  getClientByIndex(index: number) {
    return this.clientsById.get(_.keys(this.clientsById)[index])
  }

  getPlayerById(sessionId: string): Player {
    return this.state.players[sessionId]
  }

  getPlayerByIndex(index: number): Player {
    return this.state.players[_.keys(this.state.players)[index]]
  }
}
