interface Range {
	min: number
	max: number
}

interface MapObjectTemplate {
	rotation: Range // -1.0 to 1.0
	radius: Range
}

interface PlanetTemplate extends MapObjectTemplate {}

interface AsteroidBelt extends MapObjectTemplate {}

const FullRotation = { min: -1.0, max: 1.0 }
const AsteroidBeltRotation = { min: -0.3, max: 0.3 }

export interface MapObjectTemplates { [variant: number]: MapObjectTemplate }

export const PlanetTemplates: { [variant: number]: PlanetTemplate } = {
	0: { 
		radius: { min: 3, max: 5 },
		rotation: FullRotation,
	},
	1: { 
		radius: { min: 4, max: 6 },
		rotation: FullRotation,
	},
	2: { 
		radius: { min: 4.5, max: 5.5 },
		rotation: FullRotation,
	},
	3: { 
		radius: { min: 4.5, max: 5.5 },
		rotation: FullRotation,
	},
}

export const AsteroidBeltTemplates: { [variant: number]: AsteroidBelt } = {
	0: { 
		radius: { min: 2, max: 4 },
		rotation: AsteroidBeltRotation,
	},
	1: { 
		radius: { min: 3, max: 5 },
		rotation: AsteroidBeltRotation,
	},
}
