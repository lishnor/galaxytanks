import { hooks } from "@colyseus/social"

hooks.beforeAuthenticate((provider, $setOnInsert, $set) => {
    $setOnInsert.metadata = {
      coins: 60
    };
});