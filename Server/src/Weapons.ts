interface Weapon {
	damage: number
}

export const Weapons: { [variant: number]: Weapon } = {
	0: { damage: 5 },
	1: { damage: 7 },
	2: { damage: 4 },
}
