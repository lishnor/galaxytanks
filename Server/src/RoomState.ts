import { Schema, MapSchema, type } from "@colyseus/schema"
import { GameMap } from "./GameMapGenerator";

export class Player extends Schema {
    @type("number")
    tankAngle: number;

    @type("number")
    gunAngle: number;
    
    @type("int32")
    health: number;

    @type("int32")
    weaponType: number;
}

export class RoomState extends Schema {
    @type(GameMap)
    gameMap: GameMap = new GameMap();

    @type({ map: Player })
    players = new MapSchema<Player>();
}
